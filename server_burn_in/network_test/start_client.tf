
provider "baremetal" {
  tenancy_ocid         = "${var.tenancy_ocid}"
  user_ocid            = "${var.user_ocid}"
  fingerprint          = "${var.fingerprint}"
  private_key_path     = "${var.private_key_path}"
}

resource "null_resource" "startup"{
  depends_on = ["module.test-servers"]
  count = "${var.how_many}"
  provisioner "remote-exec" {
    inline = [
      "sudo systemctl stop firewalld",
      "sudo systemctl disbale firewalld",
      "bmcs os bucket create -ns oracleiad --name ${var.server_name}_${count.index} --compartment-id ${var.compartment_id}",
      "date > date.out",
      "bmcs os object put -ns oracleiad -bn ${var.server_name}_${count.index} --name startdate.txt --file date.out --force",
      "nohup ./web_server -local_net_interface ens3 -tcp_listen_port 5000 > web_server.out &",
      "sleep 5",
      "nohup ./udp_packet_generator_server -ipv4_cidr 10.0.0.0/27 -total_packets_expected 5000000 -local_net_interface ens3 -web_ip ${element(data.baremetal_core_vnic.vnics.*.private_ip_address,count.index)} -web_port 5000 -udp_listen_port 5005 > packet_server.out &",
      "sleep 5"
    ]
    connection {
      host = "${element(data.baremetal_core_vnic.vnics.*.public_ip_address,count.index)}"
      type = "ssh"
      user = "opc"
      private_key = "${file(var.private_key_path)}"
    }
  }
}

resource "null_resource" "shutdown"{
  count = "${var.how_many}"
  provisioner "remote-exec" {
    inline = [
      "bmcs os object put -ns oracleiad -bn ${var.server_name}_${count.index} --name web_service_results.db --file web_service_results.db --force",
      "bmcs os object put -ns oracleiad -bn ${var.server_name}_${count.index} --name web_service_results.db-journal --file web_service_results.db-journal --force",
      "bmcs os object put -ns oracleiad -bn ${var.server_name}_${count.index} --name web_server.out --file web_server.out --force",
      "bmcs os object put -ns oracleiad -bn ${var.server_name}_${count.index} --name packet_server.out --file packet_server.out --force",
      "bmcs os object put -ns oracleiad -bn ${var.server_name}_${count.index} --name packet_client.out --file packet_client.out --force",
    ]
    connection {
      host = "${element(data.baremetal_core_vnic.vnics.*.public_ip_address,count.index)}"
      type = "ssh"
      user = "opc"
      private_key = "${file(var.private_key_path)}"
    }
  }
}

resource "null_resource" "client"{
  count = "${var.how_many}"
  provisioner "remote-exec" {
    inline = [
      #"nohup ./udp_packet_generator_client -ipv4_cidr 10.0.0.0/26 -burst_count 5 -burst_interval 5 -packet_count 1000000 -local_net_interface ens3 -packet_size 32 -web_ip ${element(var.private_ips,count.index)} -web_port 5000 -udp_dst_port 5005 > packet_client.out &",
      "nohup ./udp_packet_generator_client -ipv4_cidr 10.0.0.0/27 -burst_count 5 -burst_interval 5 -packet_count 1000000 -local_net_interface ens3 -packet_size 32 -web_ip ${element(data.baremetal_core_vnic.vnics.*.private_ip_address,count.index)} -web_port 5000 -udp_dst_port 5005 > packet_client.out &",
      "sleep 10"
    ]
    connection {
      #host = "${element(var.public_ips,count.index)}"
      host = "${element(data.baremetal_core_vnic.vnics.*.public_ip_address,count.index)}"
      type = "ssh"
      user = "opc"
      private_key = "${file(var.private_key_path)}"
    }
  }
}

