#Parameters for API User - Update to reflect your user, and private key
private_key_path = "/Users/jcalise/.ssh/id_rsa"
fingerprint = "a0:36:45:21:af:bd:50:e6:22:85:58:fc:b6:5a:64:26"
user_ocid = "ocid1.user.oc1..aaaaaaaa2h6wbapuuuxturyhjeflqbbul62tuxbwldugnnhrw6zg3zfgpgoa"
tenancy_ocid = "ocid1.tenancy.oc1..aaaaaaaahdng7buvfq3ruyj2pbge4j2cmorgfyltktgpgskg3srq2rjddkxa"


#Paramters for server creation - Update to change how the server is created

#Number of servers to create
how_many = 16
#Shape - valid values are BM1, VM1, VM2, VM4, VM8, VM16
shapes = ["VM.Standard1.1","VM.Standard1.2","VM.Standard1.4","VM.Standard1.8", "VM.Standard1.16", "BM.Standard1.36", "BM.HighIO1.36","BM.DenseIO1.36","VM.Standard1.1","VM.Standard1.2","VM.Standard1.4","VM.Standard1.8", "VM.Standard1.16", "BM.Standard1.36", "BM.HighIO1.36","BM.DenseIO1.36"]
#Add your public key, if you need SSH access
public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC/sRt6mLpfghfGik9k+pQn6IIx/w+u7elKCWwyOi9MHYSgQMAUOLQsLQlX1L6uj3tagncYS5B7VIoAe7ltCmiu2f1x3gK7xxAhWp/m+O8DDrLG7sbEXonxfuXXrqyozlGF/L7kPjiQrcu2jBvjUlzKBctAyPcpfYmfYTmeCysnxC4Y/JPn9sLv/yFLz0Odqjcty7NJo8e779nQ8bfU5PidCADMPzNtMxXmzhw/tpbkfk98MdMjF0ffGW7wQ74kMGMhkZM/Kkb7OeaRfhEYcFKyyLhu0pmhYElU24D672ieXrYPHULfQvgIR79babOKQmryEsqz3j2DbvPXVzJoR5eX gnwright@gnwright-mac\nssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC0G5R0I21xfA0PyCFOI+TCRqSGtuEbAO9c7zRsE652jQ/LDGLS6uCL+U3eB4+e8FnnRF3A1IB9jPO7pLvhbL9nlD2PbOwqmWMp4W3a8xyjjHEcTaQ9Hc085GDtUki6hyW4+jtJ3GdK5Wp7liH438tND6EAdVeUcrt07/o99eKeDjtTd6R5AeL08JPW7OuEYLcYHH2ZkMyu795XuWAIQXeDMfbnLj6gcTgyftVZViGPoELO39Cl7g/JxVXsnNTCVtTa5CRRmaF/mKVcGuj+5fiTafx8CNh/6hkBm2hryBdTcSwGkiZgXs1GkOfmEEkk+61kNJbpHSo0FiBz1h4B91zD jamescalise@Jamess-MacBook-Pro.local"
server_name = "ubuntu_test"


#Paramters specific to the current environment.  Should not have to be changed.

#ADs
ads = ["Tjlg:US-ASHBURN-AD-1","Tjlg:US-ASHBURN-AD-2","Tjlg:US-ASHBURN-AD-3"]
#Beta VCN Subnets
subnets = ["ocid1.subnet.oc1.iad.aaaaaaaazwv3vms2z5auxpzc5ecpwlvu6ls2kpqwj3li5ctkmtvj6cz2flfa", "ocid1.subnet.oc1.iad.aaaaaaaaeneutjxz43l3h3tgfid7vixe6sfb6wtpfnzaziqxrpgmtr6rzi6a","ocid1.subnet.oc1.iad.aaaaaaaa3h6qxbocdk5q67ioi7qdhuea7m2mwnhinovhkkjfrsbf7pr43vlq"]
#Beta Compartment OCID
compartment_id = "ocid1.compartment.oc1..aaaaaaaayigbvneattwq5y5pqasxyjia6p6qyh6twzxrguyitsb7y2tz5sta"
#Ubuntu
image = "ocid1.image.oc1.iad.aaaaaaaa4odtinvezdnpf2jq7nk6mens4miwwolvpamh27b3nvjkpo5fervq"

