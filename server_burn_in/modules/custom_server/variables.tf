variable "compartment_id" {
 description = "Compartment to run server in"
}

variable "public_key" {
 description = "Public key to access the Server"
}

variable "image" {
  description = "BMC Image IDs"
}

variable "shapes" {
  type = "list"
  description = "BMC Shape IDs"
}

variable "ads" {
  description = "BMC Availability Domains"
  type = "list"
}

variable "subnet_ids" {
  description = "Network Subnets"
  type = "list" 
}

variable "display_name" {
  description = "Server Name"
}

variable "how_many" {
  description = "Count"
}
