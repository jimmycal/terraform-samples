resource "baremetal_core_instance" "custom-server" {
  count = "${var.how_many}"
  availability_domain = "${element(var.ads,count.index%length(var.ads))}"
  compartment_id = "${var.compartment_id}"
  display_name = "${var.display_name}_${count.index}"
  image = "${var.image}"
  shape = "${element(var.shapes, count.index%length(var.shapes))}"
  subnet_id = "${element(var.subnet_ids, count.index%length(var.subnet_ids))}"
  metadata {
    ssh_authorized_keys = "${var.public_key}"
  }
}

data "baremetal_core_vnic_attachments" "vnics" {
      count = "${var.how_many}"
      compartment_id = "${var.compartment_id}"
      #availability_domain = "${lookup(var.ad_name[count.index%3],"name")}"
  availability_domain = "${element(var.ads,count.index%length(var.ads))}"
  instance_id = "${element(baremetal_core_instance.custom-server.*.id,count.index)}"
}
