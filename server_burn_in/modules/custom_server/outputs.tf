output "ocids" {
  value = ["${baremetal_core_instance.custom-server.*.id}"]
}

output "vnic_ids" {
  value = ["${data.baremetal_core_vnic_attachments.vnics.*.vnic_attachments.0.vnic_id}"]
}

output "display_names" {
  value = ["${baremetal_core_instance.custom-server.*.display_name}"]
}
