provider "baremetal" {
  tenancy_ocid         = "${var.tenancy_ocid}"
  user_ocid            = "${var.user_ocid}"
  fingerprint          = "${var.fingerprint}"
  private_key_path     = "${var.private_key_path}"
}

data "baremetal_identity_availability_domains" "ads" {
  compartment_id = "${var.tenancy_ocid}"
}

module "test-servers" {
  source = "./modules/custom_server"
  how_many = "${var.how_many}"
  compartment_id = "${var.compartment_id}"
  image = "${var.image}"
  ads = "${var.ads}"
  display_name = "${var.server_name}"
  shapes = "${var.shapes}"
  subnet_ids = "${var.subnets}"
  public_key = "${var.public_key}"
}

data "baremetal_core_vnic" "vnics" {
   count = "${var.how_many}"
   vnic_id = "${element(module.test-servers.vnic_ids, count.index)}"
}

output "public_ip_address" {
  value = ["${data.baremetal_core_vnic.vnics.*.public_ip_address}"]
}

output "private_ip_address" {
  value = ["${data.baremetal_core_vnic.vnics.*.private_ip_address}"]
}

output "hostnames" {
  value = "${module.test-servers.display_names}"
}
