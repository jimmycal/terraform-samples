#Parameters for API User - Update to reflect your user, and private key
private_key_path = "/Users/jcalise/.ssh/id_rsa"
fingerprint = "a0:36:45:21:af:bd:50:e6:22:85:58:fc:b6:5a:64:26"
user_ocid = "ocid1.user.oc1..aaaaaaaa2h6wbapuuuxturyhjeflqbbul62tuxbwldugnnhrw6zg3zfgpgoa"
tenancy_ocid = "ocid1.tenancy.oc1..aaaaaaaahdng7buvfq3ruyj2pbge4j2cmorgfyltktgpgskg3srq2rjddkxa"

#Paramters for server creation - Update to change how the server is created
#Number of servers to create
how_many = 3
#Shape - valid values are BM1, VM1, VM2, VM4, VM8, VM16
shape = "VM2"
#Add your public key, if you need SSH access
public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC0G5R0I21xfA0PyCFOI+TCRqSGtuEbAO9c7zRsE652jQ/LDGLS6uCL+U3eB4+e8FnnRF3A1IB9jPO7pLvhbL9nlD2PbOwqmWMp4W3a8xyjjHEcTaQ9Hc085GDtUki6hyW4+jtJ3GdK5Wp7liH438tND6EAdVeUcrt07/o99eKeDjtTd6R5AeL08JPW7OuEYLcYHH2ZkMyu795XuWAIQXeDMfbnLj6gcTgyftVZViGPoELO39Cl7g/JxVXsnNTCVtTa5CRRmaF/mKVcGuj+5fiTafx8CNh/6hkBm2hryBdTcSwGkiZgXs1GkOfmEEkk+61kNJbpHSo0FiBz1h4B91zD jamescalise@Jamess-MacBook-Pro.local"

#Paramters specific to the current environment.  Should not be changed.

#Beta VCN Subnets
subnets = ["ocid1.subnet.oc1.phx.aaaaaaaawvu2rqzxqjvehnffdk6ssutlxmibdw44nhdo2msebqkpo2bzs4wq", "ocid1.subnet.oc1.phx.aaaaaaaalqffvz76itrtbmnghrbna4cdvro4ilpmeajgbuucwv4x2eswd3ta","ocid1.subnet.oc1.phx.aaaaaaaa2fdgdwa5ypoqxho645q7rklbuocpl5mbyehdq75nedrxr4l4pcqq"]
#Beta Compartment OCID
compartment_id = "ocid1.compartment.oc1..aaaaaaaayigbvneattwq5y5pqasxyjia6p6qyh6twzxrguyitsb7y2tz5sta"
#Using custom image that has CLI installed and writes data to object storage from the server before destroy
image = "ocid1.image.oc1.phx.aaaaaaaalste2ekr3nqckyifciztkkk2vxeqxei5hlmtsukh6nglz3hrbniq"