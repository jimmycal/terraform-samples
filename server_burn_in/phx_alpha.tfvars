#Parameters for API User - Update to reflect your user, and private key
private_key_path = "/Users/jcalise/.ssh/id_rsa"
fingerprint = "a0:36:45:21:af:bd:50:e6:22:85:58:fc:b6:5a:64:26"
user_ocid = "ocid1.user.oc1..aaaaaaaa2h6wbapuuuxturyhjeflqbbul62tuxbwldugnnhrw6zg3zfgpgoa"
tenancy_ocid = "ocid1.tenancy.oc1..aaaaaaaahdng7buvfq3ruyj2pbge4j2cmorgfyltktgpgskg3srq2rjddkxa"


#Paramters for server creation - Update to change how the server is created

#Number of servers to create
how_many =9
#Shape - valid values are BM1, VM1, VM2, VM4, VM8, VM16
shapes = ["VM.Standard1.1","VM.Standard1.4","VM.Standard1.8","VM.Standard1.16","BM.Standard1.36"]
#Add your public key, if you need SSH access
public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC0G5R0I21xfA0PyCFOI+TCRqSGtuEbAO9c7zRsE652jQ/LDGLS6uCL+U3eB4+e8FnnRF3A1IB9jPO7pLvhbL9nlD2PbOwqmWMp4W3a8xyjjHEcTaQ9Hc085GDtUki6hyW4+jtJ3GdK5Wp7liH438tND6EAdVeUcrt07/o99eKeDjtTd6R5AeL08JPW7OuEYLcYHH2ZkMyu795XuWAIQXeDMfbnLj6gcTgyftVZViGPoELO39Cl7g/JxVXsnNTCVtTa5CRRmaF/mKVcGuj+5fiTafx8CNh/6hkBm2hryBdTcSwGkiZgXs1GkOfmEEkk+61kNJbpHSo0FiBz1h4B91zD jamescalise@Jamess-MacBook-Pro.local"


#Paramters specific to the current environment.  Should not have to be changed.

#Alpha VCN Subnets
subnets = ["ocid1.subnet.oc1.phx.aaaaaaaag54hc2qoq33piwber6pjn3lwjsl47lqhnlgxa3ioc5mhrogduwiq", "ocid1.subnet.oc1.phx.aaaaaaaax2zehhhc5inw2alycsunvzrjpb3nvnesxcotrvwm7tz6ybzjmd4q","ocid1.subnet.oc1.phx.aaaaaaaazdg2pbkexdiews3spok3um2qw62cewgssrgfyvdr2hbslgjswosq"]
#Alpha Compartment OCID
compartment_id = "ocid1.compartment.oc1..aaaaaaaajficfh3l5gk3wbu3es5y4pzkcehq32v7ewk5w542e4vgopqbyb4a"
#Using custom image that has CLI installed and writes data to object storage from the server before destroy
image = "ocid1.image.oc1.phx.aaaaaaaalste2ekr3nqckyifciztkkk2vxeqxei5hlmtsukh6nglz3hrbniq"